Browser Clock
=====

A simple analog clock for your browser. This project is meant to gain some experience with the [Zdog](https://zzz.dog/) library for javascript. Zdog renders basic shapes in pseudo-3D, and provides easy controls to transform these shapes.

# Development

You must have `npm` and `nodejs` installed. To begin development, clone this project and enter the project's directory.

Run the development server:

```
$ npm run start
```

# Building for Production

To build browser-clock for production, run the following:

```
$ npm run build
```

A directory called `dist` will be created with some static content.
