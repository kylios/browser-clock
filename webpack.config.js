const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = function(env, argv) {
  return {
    mode: argv.mode,
    entry: {
      app: './src/index.js'
    },
    devtool: argv.mode === 'production' ? 'inline-source-map' : 'eval',
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'index.html'
      })
    ],
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    module: {
      rules: [
        {
          test: require.resolve('./node_modules/zdog/js/dragger.js'),
          use: 'imports-loader?this=>window'
        },
        {
          test: /\.(ttf)$/,
          use: [
            'file-loader'
          ]
        }
      ]
    }
  }
}
