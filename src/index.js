import Zdog from 'zdog'

import Clock from './Clock'

function ClockTwo(illo) {
  const RADIUS = 160
  const SECOND_HAND_LENGTH_RATIO = 0.9
  const MINUTE_HAND_LENGTH_RATIO = 0.6
  const HOUR_HAND_LENGTH_RATIO = 0.4
  const SECOND_HAND_WIDTH = 4
  const MINUTE_HAND_WIDTH = 8
  const HOUR_HAND_WIDTH = 8
  const NUMBER_HEIGHT = 16

  let date = new Date()

  const clock = new Zdog.Ellipse({
    addTo: illo,
    diameter: RADIUS * 2,
    color: 'white',
    stroke: 1,
    fill: true
  })

  // Clock outline
  new Zdog.Ellipse({
    addTo: clock,
    diameter: RADIUS * 2,
    color: 'black',
    stroke: 1,
    fill: false
  })

  // Draw numbers
  for (let i = 0; i < 12; i++) {
    new Zdog.Shape({
      addTo: illo,
      color: 'black',
      stroke: 3,
      path: [
        { y: RADIUS },
        { y: RADIUS - NUMBER_HEIGHT }
      ],
      rotate: {
        z: i * (Zdog.TAU / 12.0)
      }
    })

    for (let j = 1; j < 5; j++) {
      new Zdog.Shape({
        addTo: illo,
        color: 'black',
        stroke: 1,
        path: [
          { y: RADIUS },
          { y: RADIUS - NUMBER_HEIGHT / 2.0 }
        ],
        rotate: {
          z: i * (Zdog.TAU / 12.0) + j * (Zdog.TAU / 60.0)
        }
      })
    }
  }

  function instantiateHand(width, length) {
    return new Zdog.Shape({
      addTo: illo,
      path: [
        { y: 0 },
        {
          x: -1 * (width / 2.0),
          y: -1 * (length / 5.0)
        },
        { y: -1 * length },
        {
          x: width / 2.0,
          y: -1 * (length / 5.0)
        }
      ],
      fill: true,
      stroke: 1,
      color: 'black'
    })
  }

  const secondHandLength = SECOND_HAND_LENGTH_RATIO * RADIUS - NUMBER_HEIGHT
  const secondHand = instantiateHand(SECOND_HAND_WIDTH, secondHandLength)

  const minuteHandLength = MINUTE_HAND_LENGTH_RATIO * RADIUS - NUMBER_HEIGHT
  const minuteHand = instantiateHand(MINUTE_HAND_WIDTH, minuteHandLength)

  const hourHandLength = HOUR_HAND_LENGTH_RATIO * RADIUS - NUMBER_HEIGHT
  const hourHand = instantiateHand(HOUR_HAND_WIDTH, hourHandLength)

  this.update = function() {
    date = new Date()

    const seconds = date.getSeconds()
    const minutes = date.getMinutes()
    const hours = date.getHours()

    secondHand.rotate.z = (seconds / 60.0) * Zdog.TAU
    minuteHand.rotate.z = (minutes / 60.0 + seconds / 3600.0) * Zdog.TAU
    hourHand.rotate.z = ((hours % 12 + minutes / 60.0) / 12) * Zdog.TAU
  }
}

function main() {
  const illo = new Zdog.Illustration({
    element: '.zdog-canvas',
    zoom: 1,
    dragRotate: true,
  })

  const clock = new Clock(illo)

  illo.updateRenderGraph()

  function animate() {
    clock.update()
    illo.updateRenderGraph()
    requestAnimationFrame(animate)
  }

  animate()
}

main()
