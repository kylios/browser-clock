import Zdog from 'zdog'
import Zfont from 'zfont'
import ptSerif from './PT_Serif-Web-Regular.ttf'

Zfont.init(Zdog)

const HEIGHT = 12
const COLOR = 'black'

const font = new Zdog.Font({
  src: ptSerif
})

export function makeNumber(illo, number) {
  return new Zdog.Text({
    addTo: illo,
    font: font,
    value: number,
    fontSize: HEIGHT,
    color: COLOR
  })
}
