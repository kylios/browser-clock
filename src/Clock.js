import Zdog from 'zdog'
import { makeNumber } from './Numbers'

const DEBUG = false

const RADIUS = 160
const SECOND_HAND_LENGTH_RATIO = 0.9
const MINUTE_HAND_LENGTH_RATIO = 0.6
const HOUR_HAND_LENGTH_RATIO = 0.4
const SECOND_HAND_WIDTH = 2
const MINUTE_HAND_WIDTH = 4
const HOUR_HAND_WIDTH = 8
const NUMBER_OFFSET = 16
const TICK_ANGLE = (Zdog.TAU / 60.0)

class Clock {
  constructor(illo) {
    const clock = new Zdog.Anchor({
      addTo: illo
    })

    this.clock = clock

    const {
      p0: bigP0,
      p1: bigP1,
      p2: bigP2,
      p3: bigP3
    } = this._getPointsForTicks(TICK_ANGLE)

    const {
      p0: smallP0,
      p1: smallP1,
      p2: smallP2,
      p3: smallP3
    } = this._getPointsForTicks(2.0 * TICK_ANGLE / 3.0)

    for (let i = 0; i < 12; i++) {
      // https://www.tinaja.com/glib/bezarc1.pdf
      // https://www.tinaja.com/glib/bezcirc2.pdf

      if (DEBUG) {
        this._plot_debug_points(clock, p0, p1, p2, p3, i)
      }

      for (let j = 1; j < 5; j++) {
        new Zdog.Shape({
          addTo: clock,
          color: 'black',
          stroke: 1,
          closed: false,
          path: [
            smallP0,
            {
              bezier: [
                smallP1,
                smallP2,
                smallP3
              ]
            }
          ],
          rotate: {
            z: i * (Zdog.TAU / 12.0) + j * (Zdog.TAU / 60.0)
          }
        })
      }

      new Zdog.Shape({
        addTo: clock,
        color: DEBUG ? `rgb(${255 * i / 12.0}, 0, 0)` : 'black',
        stroke: 3,
        closed: false,
        path: [
            bigP0,
            {
              bezier: [
                bigP1,
                bigP2,
                bigP3
              ]
            }
        ],
        rotate: {
          z: i * (Zdog.TAU / 12.0)
        }
      })
    }

    this.secondHand = this._instantiateHand(SECOND_HAND_WIDTH)
    this.minuteHand = this._instantiateHand(MINUTE_HAND_WIDTH)
    this.hourHand = this._instantiateHand(HOUR_HAND_WIDTH)

    this._drawNumbers()
  }

  update() {
    const date = new Date()

    const seconds = date.getSeconds()
    const minutes = date.getMinutes()
    const hours = date.getHours()

    this.secondHand.rotate.z = (seconds / 60.0) * Zdog.TAU
    this.minuteHand.rotate.z = (minutes / 60.0 + seconds / 3600.0) * Zdog.TAU
    this.hourHand.rotate.z = ((hours % 12 + minutes / 60.0) / 12) * Zdog.TAU
  }

  _getXYForClockPosition(angle, hypotenuse) {
    const adjustedAngle = Zdog.TAU / 2 - angle
    return [
      hypotenuse * Math.sin(adjustedAngle),
      hypotenuse * Math.cos(adjustedAngle)
    ]
  }

  _drawNumbers() {
    for (var i = 1; i < 13; i++) {
      const num = makeNumber(this.clock, `${i}`)

      const [ x, y ] = this._getXYForClockPosition(i * Zdog.TAU / 12, RADIUS + NUMBER_OFFSET)
      num.translate.x = x
      num.translate.y = y
    }
  }

  _instantiateHand(extraWidth) {
    const hand = new Zdog.Anchor({
      addTo: this.clock,
    })
    new Zdog.Ellipse({
      addTo: hand,
      color: 'red',
      stroke: 1,
      quarters: 2,
      diameter: 2 * RADIUS,
      quarters: 2,
      rotate: {
        x: Zdog.TAU / 4.0,
        y: Zdog.TAU / 4.0 // 90 degrees
      }
    })

    // Make the hands look pointy
    while (extraWidth--) {
      const angle = Math.atan(extraWidth / (1.0 * RADIUS))

      new Zdog.Ellipse({
        addTo: new Zdog.Anchor({
          addTo: hand,
          translate: {
            x: -1 * extraWidth
          },
          rotate: {
            z: angle
          }
        }),
        color: 'red',
        stroke: 1,
        diameter: 2 * RADIUS,
        quarters: 1,
        rotate: {
          y: Zdog.TAU / 4.0, // 90 degrees
        }
      })
      new Zdog.Ellipse({
        addTo: new Zdog.Anchor({
          addTo: hand,
          translate: {
            x: extraWidth
          },
          rotate: {
            z: -1 * angle
          }
        }),
        color: 'red',
        stroke: 1,
        diameter: 2 * RADIUS,
        quarters: 1,
        rotate: {
          y: Zdog.TAU / 4.0, // 90 degrees
        }
      })
    }

    return hand
  }

  _getPointsForTicks(angle) {
    const p0 = {
      x: 0,
      y: Math.cos(angle) * RADIUS,
      z: Math.sin(angle) * RADIUS
    }
    const p1 = {
      x: 0,
      y: RADIUS + (4.0 - Math.cos(angle)) / 3.0,
      z: ((1.0 - Math.cos(angle)) * (Math.cos(angle) - 3.0)) / (3.0 * Math.sin(angle)),
    }
    const p2 = {
      x: 0,
      y: RADIUS + (4.0 - Math.cos(angle)) / 3.0,
      z: -1 * ((1.0 - Math.cos(angle)) * (Math.cos(angle) - 3.0)) / (3.0 * Math.sin(angle))
    }
    const p3 = {
      x: 0,
      y: Math.cos(angle) * RADIUS,
      z: -1 * Math.sin(angle) * RADIUS
    }

    return { p0, p1, p2, p3 }
  }

  _plot_debug_points(clock, p0, p1, p2, p3, i) {

      new Zdog.Hemisphere({
        addTo: clock,
        color: 'gray',
        stroke: 1,
        diameter: 2 * RADIUS,
        backface: 'white'
      })

      new Zdog.Shape({
        addTo: clock,
        color: 'green',
        stroke: 1,
        path: [
          {x: 0, y: 0},
          {x: 0, y: RADIUS}
        ],
        rotate: {
          z: i * (Zdog.TAU / 12.0)
        }
      })
    new Zdog.Shape({
      addTo: clock,
      color: 'blue',
      stroke: 5,
      path: [p0],
      rotate: {
        z: i * (Zdog.TAU / 12.0)
      }
    })
    new Zdog.Shape({
      addTo: clock,
      color: 'blue',
      stroke: 5,
      path: [p1],
      rotate: {
        z: i * (Zdog.TAU / 12.0)
      }
    })
    new Zdog.Shape({
      addTo: clock,
      color: 'blue',
      stroke: 5,
      path: [p2],
      rotate: {
        z: i * (Zdog.TAU / 12.0)
      }
    })
    new Zdog.Shape({
      addTo: clock,
      color: 'blue',
      stroke: 5,
      path: [p3],
      rotate: {
        z: i * (Zdog.TAU / 12.0)
      }
    })
  }
}

export default Clock
